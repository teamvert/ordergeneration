import { NbMenuService } from '@nebular/theme';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TouchSequence } from 'selenium-webdriver';
import { OrdersService } from '../../../@core/data/orders.service';
//import { CreateAccountModalComponent } from './create-account-modal/create-account-modal.component';


@Component({
  selector: 'ngx-not-found',
  styleUrls: ['./order.component.scss'],
  templateUrl: './order.component.html',
})
export class OrderComponent implements OnDestroy, OnInit {
  
  private alive = true;

  type = 'month';
  types = ['week', 'month', 'year'];
  currentTheme: string;

  order: any = {};
  itemQtyArray: any = [];
  counter: number =0;
  totalqty: number = 0;
  totalPercentage: number = 0;
  percentAlert: boolean =false;
  disable: boolean = true;
  orderedqty: number;
  errors: string[] = [];
  messages: string[] = [];

  constructor(private themeService: NbThemeService,
               protected orderservice: OrdersService,
              private menuService: NbMenuService,
              private modalService: NgbModal) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.currentTheme = theme.name;
    });
  }

  ngOnInit() {
   
  }



  ngOnDestroy() {
    this.alive = false;
  }
  

  sendOrderData(){
    this.disable = true;
    this.order.itemQtyArray = this.itemQtyArray;
    this.order.noOfBatch = 1;
    console.log(this.order);
    this.orderservice.sendOrder(this.order).subscribe(out => {
      
      //console.log(out);
      this.messages = ["Success! Please monitor Logger for progress."];
        },
        err => {
          //console.log(err)
          this.errors = ["Error Occured!"]
    }
    )   
  }


  addSku(newSku: string, newQty: number) {
   // console.log(newSku);
   // console.log(newQty);
    newQty = Number(newQty);
    this.counter += 1;
    this.totalqty += newQty;

    this.itemQtyArray.push({'id': this.counter,'item': newSku, 'qty': newQty});
    //console.log(this.itemQtyArray);
    this.disableFunction();
  }

  deleteSku(id){
    for (var i=0; i<this.itemQtyArray.length; i++) {
      if (this.itemQtyArray[i]['id']===id) {
        this.totalqty -= this.itemQtyArray[i]['qty']
        this.itemQtyArray.splice(i,1)
       // this.validate();
      }
    }
    this.disableFunction();
  }

  validatePercentage(){
    this.totalPercentage = (this.order.pL1Q1 + this.order.pL1Q2 + this.order.pL2Q1 + this.order.pL3Q1 + this.order.pL4Q1);
   // console.log(this.totalPercentage)
    if(this.totalPercentage != 100 || this.order.pL1Q1 == null || this.order.pL1Q2 == null || this.order.pL2Q1 == null || this.order.pL3Q1 == null || this.order.pL4Q1 == null) {
      this.percentAlert = true;
    } else {
      this.percentAlert = false;
    }

  //  console.log(this.percentAlert);
    this.calculateqty();
    this.disableFunction();
  }

  calculateqty() {

    this.orderedqty =   (((this.order.pL1Q1/100) * 1 * 1 * this.order.orderCtr)
                            + ((this.order.pL1Q2/100) * 1 * 2 * this.order.orderCtr)
                            + ((this.order.pL2Q1/100) * 2 * 1 * this.order.orderCtr)
                            + ((this.order.pL3Q1/100) * 3 * 1 * this.order.orderCtr)
                            + ((this.order.pL4Q1/100) * 4 * 1 * this.order.orderCtr))
   // console.log(this.orderedqty)    
    this.disableFunction();                    
  }




  disableFunction() {
   // console.log(this.itemQtyArray)
    //totalPercentage != 100 || order.orderCtr == null || order.orderCtr < 1 
    //console.log((this.order.pL1Q1 + this.order.pL1Q2 + this.order.pL2Q1 + this.order.pL3Q1 + this.order.pL4Q1))
    this.disable = (this.order.orderCtr == null || this.order.orderCtr < 1 || (this.order.pL1Q1 + this.order.pL1Q2 + this.order.pL2Q1 + this.order.pL3Q1 + this.order.pL4Q1) != 100 ||
                    this.order.pL1Q1 == null || this.order.pL1Q2 == null || this.order.pL2Q1 == null || this.order.pL3Q1 == null || this.order.pL4Q1 == null ||
                    this.itemQtyArray.length == 0 || 
                    this.totalqty < this.orderedqty);
  }


  reset() {
    this.order = {};
    this.itemQtyArray = [];
    this.counter =0;
    this.totalqty = 0;
    this.totalPercentage = 0;
    this.percentAlert =true;
    this.messages = [];
    this.errors = [];

    this.disableFunction();

  }
}
