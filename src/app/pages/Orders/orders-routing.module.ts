import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersComponent } from './orders.component';
import { OrderComponent } from './order/order.component';

const routes: Routes = [{
  path: '',
  component: OrdersComponent,
  children: [{
    path: 'order',
    component: OrderComponent,
  },{
    path: '',
    redirectTo: 'order',
    pathMatch: 'full',
  },],
  
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdersRoutingModule { }

export const routedComponents = [
  OrdersComponent,
  OrderComponent,
];
