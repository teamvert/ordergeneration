import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { OrdersRoutingModule, routedComponents } from './orders-routing.module';
//import { CreateAccountModalComponent } from './order/create-account-modal/create-account-modal.component';

@NgModule({
  imports: [
    ThemeModule,
    OrdersRoutingModule,
  ],
  declarations: [
    ...routedComponents,
    //CreateAccountModalComponent
  ],
  entryComponents: [
   // CreateAccountModalComponent
  ]
})
export class OrdersModule { }
