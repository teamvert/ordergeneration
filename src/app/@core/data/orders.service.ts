import { Injectable } from '@angular/core';
//import { Observable, Subject } from 'rxjs';
import { delay, share } from 'rxjs/operators';
import { of as observableOf, Observable, Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';

import 'rxjs/add/operator/map';

@Injectable()
export class OrdersService {
 sessionToken: string;
 constructor (private authService: NbAuthService, private http: HttpClient) {
   this.authService.onTokenChange()
   .subscribe((token: NbAuthJWTToken) => {
       this.sessionToken = token.getValue();
   });
};
// protected layoutSize$ = new Subject();


sendOrder(data){
  return this.http.post('http://18.191.105.49:9000/api/Order/generate',data,  { responseType: 'text'}).map(res =>{
       ////console.log(res)
  })
}

}