import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Created by <b><a href="http://projectverte.com/" target="_blank">Verte</a></b> 2019</span>
    
  `,
})
export class FooterComponent {
}
